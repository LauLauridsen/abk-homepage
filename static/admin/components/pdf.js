export default {
  // Internal id of the component
  id: "pdf",
  // Visible label
  label: "PDF",
  // Fields the user need to fill o ut when adding an instance of the component
  fields: [
    {
      name: "file",
      label: "PDF to display",
      widget: "file",
      // pattern: [/.+.pdf$/, "Must be a pdf file"],
      required: true,
    },
  ],
  // Regex pattern used to search for instances of this block in the markdown document.
  pattern: /^{{< pdf \"(.+)\" >}}/,
  // This is used to populate the custom widget in the markdown editor in the CMS.
  fromBlock: function (match) {
    return {
      file: match[1],
    };
  },
  // This is used to serialize the data from the custom widget to the
  // markdown document
  toBlock: function (data) {
    return `{{< pdf "${data.file}" >}}`;
  },
  // Preview output for this component. Can either be a string or a React component
  // (component gives better render performance)
  toPreview: function (data) {
    return `{{< pdf "${data.file}" >}}`;
  },
};
