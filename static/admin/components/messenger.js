export default {
    // Internal id of the component
    id: "messenger-button",
    // Visible label
    label: "Messenger Button",
    // Fields the user need to fill o ut when adding an instance of the component
    fields: [
      {
        name: 'page',
        label: 'Id of the facebook page',
        widget: 'string'
      },
    ],
    // Regex pattern used to search for instances of this block in the markdown document.
    pattern: /{{<\s(messengerbutton)\s(.*)\s>}}/,
    // This is used to populate the custom widget in the markdown editor in the CMS.
    fromBlock: function(match) {
      return {
        page: match[2],
      };
    },
    // This is used to serialize the data from the custom widget to the
    // markdown document
    toBlock: function(data) {
      return `{{< messengerbutton ${data.page} >}}`;
    },
    // Preview output for this component. Can either be a string or a React component
    // (component gives better render performance)
    toPreview: function(data) {
      return `
    <a href="https://m.me/${data.page}?ref=abk_website" class="messenger-button">
        <span class="mdi mdi-facebook-messenger"></span> Message Us</a
    >
      `;
    }
  }