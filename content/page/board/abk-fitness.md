---
title : ABK-Fitness
bigimg: [{src: "/page/page-images/abk-photos/fitness.jpg", desc: ""}]
---

The ABK-Fitness board does not exist yet, but it is maybe under way.

If you are interested in the future of ABK's fitness facilities or want to
improve it, contact the [Resident's Council](/page/board/council/).
