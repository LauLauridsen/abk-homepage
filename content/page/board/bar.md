---
title: ABK-Baren
bigimg:
  - src: /media/beer.jpeg
tags:
  - ABK-Bar
---

ABK-Bar is the "party committee" of ABK Aalborg, and is in charge of Friday bars
and other social events, and is located in the
_[common house](/page/facilities/common/)_. Examples of social events held by the
ABK-Bar includes: 
[Friday bars](/page/social/friday/), [Summer Party](/page/social/summerparty/),
[Julefrokost](/page/social/christmas/), [Tour de Chambre (TDC)](/page/social/tdc/),
Beer Pong Tournament, and much more.

## What does ABK-Bar offer?

The bar offers draft beer, bottled beer, water, drinks, non-alcoholic drinks,
soft drinks, and shots at student friendly prices!

You are allowed to bring non-ABK friends to the
[Friday bars](/page/social/friday/), but for our other events are ABK residents
only, unless otherwise specified. For non-ABK bar events see the
[common house rules](/page/facilities/common/#using-the-common-house) regarding
non-ABK residents.

[The Friday bar](/page/social/friday/) is open when the _Friday bar_ sign is out
under block 5, which often is Friday from 20:00.
Bring your fellow residents to the Bar, and enjoy your evening!

## Contact

There are several ways of contacting ABK-Bar.

* You can contact us via email on
  [bar@abk-aalborg.dk](mailto:bar@abk-aalborg.dk).
* You can put a note in "Foreslagskassen" at events. "Foreslagskassen" is
  reviewed at every meeting.
* You can attend our monthly open meetings.

If you have any good ideas or comments, or if you want to join the ABK-Bar, feel
free to contact us!

## ABK-Bar Calendar

{{< calendar aa9590e0b20d202dc7123217331aacbe23e28fc7cd9b8499ce3d17ea4f45353e0e80ade2a5c51a8f446c02f68cb9c84b Bar >}}

## ABK-Bar Committee

We hold a meeting every first Tuesday of the month, except for January, July,
and August. The meetings take place in the
_[common house](/page/facilities/common/)_ at 20:00, and are open for all. So if
you want to join and help ABK-Bar, feel free to join our meetings!

As of this moment, the board of ABK-Bar consists of:

| Name                         | Apartment Number | Role          |
| ---------------------------- | ---------------- | ------------- |
| Christine Birkemark Pedersen | 83               | Chairman      |
| Jonas N. Terp                | 40               | Vice Chairman |
| Frederik Daasbjerg           | 74               | Treasurer     |
| Mathias Andresen             | 3                | Board Member  |
| Rikke Andersen               | 113              | Board Member  |
| Philip Østergaard Henriksen  | 67               | Board Member  |
| Ida Landbo Olesen            | 67               | Board Member  |
| Liv Boe Thrysøe              | 3                | Board Member  |
| Emilie Boedker               | 52               | Board Member  |
| Kathrine Rix Sørensen        | 27               | Board Member  |

## Articles of Association

Right now, the ABK-Bar's Articles are only available in danish, and can be seen
in the PDF below.

{{< pdf "/pdf/abk-bar-statues.pdf">}}
