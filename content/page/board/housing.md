---
title : Housing Association
---

The ABK-Kollegium is an _independent_ institution set up as a youth residence
with public support. ABK is subject to the rules for
["Executive Order on Organization of Youth Housing, which is listed with public support (BEK no 666 of 27/09/1991)"](https://www.retsinformation.dk/Forms/R0710.aspx?id=54781).

## Board of Directors

The Board of Directors is the college's supreme authority and makes the final
decision in major decisions about the college. ABK's Board of Directors consists
of 6 members:

- 2 members from educational institutions in Aalborg.
- 2 members from the city council in Aalborg.
- 2 members from the [Residents' Council](/page/board/council/#members).

The Board of Directors is responsible for the overall management of the ABK
Kollegium, and it is ultimately the Board that can take the decision in almost
all matters concerning the dormitory. There is a tradition on the ABK, that the
residents of the board of directors, receives their wishes if it is within
_reasonable_ limits. Board meetings are held _twice_ a year.

## Administration / Caretaker

To handle the administration and operation of the ABK Kollegium, the Board of
Directors has made an agreement with
[Hasseris Boligselskab](/page/board/housing/). Hasseris Boligselskab has
employed [a caretaker and a caretaker assistant](/page/info/contact/) who
handles the daily operations at ABK. The caretaker has an office in the same
building as the common house. 

In addition to [the caretaker](/page/info/contact/),
[Hasseris Boligselskab](/page/board/housing/) has administrative employees who
contribute to the administration, technical exporting and more.
