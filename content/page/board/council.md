---
title: Residents' council
tags:
  - ResidentsCouncil
---
The Residents' Council is there for the residents. We do what we can to make the
atmosphere and daily life at the college the best possible, by trying to keep a
close contact between the board, residents, associations at the dormitory, and
of course [the caretaker](/page/info/contact/).

The Residents' Council consists of 9 unpaid members.

## Responsible For

* Taking care of good ideas from the residents
* Suggestions for improvements / new acquisitions
* Inquiries about subsidies for associations
* Handling of complaints regarding violations of the dormitory regulations
* Opening the [dormitory's two storage rooms](/page/facilities/storage/)

Lending out: 

* [The common house](/page/facilities/common/) for private events
* [Urban gardens](/page/facilities/garden/)
* [Sewing machine, Jack (donkraft), Car ramps, and more](/page/info/equipment/)

## Contact

In order to maintain a close contact, we urge all residents to contact us if
they have questions, good ideas for initiatives at the college or generally
topics they would like the board to take up.

You are *always* welcome to contact the Residents' Council. This is done by either:
* Contacting one of the council's members
* [Meeting up to a resident council meeting](#meetings)
* Sending a Facebook message by clicking the button below. 

{{< messengerbutton abkresidentscouncil >}}

If you prefer email, the resident council can also be contacted at
[beboerraadet@abk-aalborg.dk](mailto:beboerraadet@abk-aalborg.dk).

It should be noted that meeting up personally at one of the members' apartment
_does not_ guarantee that the storage room can be opened by this kind of
personal inquiry.

# Calendar

{{< calendar aa9590e0b20d202dce07054f5578e89e553022fa538841c993aac605ab4f1e4e3a60c52075e749cdd38cabaf8c6ad5e9 Beboerrådet >}}

## Residents' Council Members

Currently, the Residents' Council consists of, elected on Annual general meeting
2022 8. December:

| Name              | Apartment (Block) | Role                         |
| ----------------- | ----------------- | ---------------------------- |
| Mathias Andresen  | 3 (7)             | Chairman, Board-member       |
| Rikke E. Andersen | 113 (5)           | Vice chairman,  Board 1. Sup |
| Jonas N. Terp     | 40 (7)            | Treasurer, Board 2. Sup      |
| Lau Lauridsen     | 69 (5)            | Board-Vice chairman          |
| Lauri Leemet      | 197 (3)           | Member                       |
| Kristian Woxholt  | 14 (7)            | Member                       |
| Sazvan Salih      | 43 (7)            | Member                       |
| Rasmus B. Larsen  | 182 (3)           | Member                       |
| Adam Anzel        | 56 (7)            | Member                       |
| Camilla Weje      |                   | 1﻿. Sup                      |
| Mikkel Sørensen   | 116 (5)           | 2. Sup                       |
| D﻿aniel Dharampal | 95 (5)            | 3. Sup                       |

# Meetings

The Residents' Council meetings are held on the _first Tuesday of each month_,
at. 17.00 in the common house and are open to all residents.

[{{< mdi folder >}} Meeting minutes](https://drive.google.com/drive/folders/1gHe5ga6ODvqHyKyb9GrsSIEOlM26mB9m?usp=sharing) also contains general resident meetings (beboermøder)

# Rules of Procedure

Right now, the Residents' Councils articles are only available in danish, and
can be read in the pdf below.

{{< pdf "../../../pdf/forretningsorden.pdf" >}}
