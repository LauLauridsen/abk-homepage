---
title: Workshop
bigimg: [{src: "/page/page-images/abk-photos/værksted.jpg", desc: ""}]
---

You can find the workshop in the garage next to Block 7. The workshop can be
used for repairing your bike/scooter/motorcycle, assembling furniture, painting
the carnival wagon etc. 

The workshop has: 

- Vice (skruestik)
- Compressor
- Bicycle stand 
- Can of oil and oil absorber

You have to bring your own tools, and remember to **clean up after your self!**

![Workshop](/page/page-images/abk-photos/værksted2.jpg)

## Items left behind

Unfortunately we experience that many items are left behind in the workshop. 
If you have taken apart your bike, lawnmower, or scooter, remember to not leave
any items behind after use. Items left behind will be **removed** unless an
agreement is made with the Residents' Council or the caretaker.
