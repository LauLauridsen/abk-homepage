---
title: Laundry Room
bigimg: [{src: "/page/page-images/abk-photos/vaskerum.jpg", desc: ""}]
---

You can find the laundry room in the basement under Block 7.

It costs _10,- kr._ to use a washing machine, and _4,- kr._ to use a tumble
dryer for 10 minutes.

You can also find an iron and ironing table that is free to use, as well as a
red machine for coloring clothes.
