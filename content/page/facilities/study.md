---
title: Study Room
---

If you need a place where you can immerse yourself in your work, the study room
is ready for use. You will find the study room in block 5, through the door
under the stairs next to the mailboxes. It is expected that when you make use of
the room, you respect its other users.
