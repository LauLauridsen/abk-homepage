---
title: Storage Rooms
bigimg: [{src: "/page/page-images/box.jpg", desc: ""}]
---

The Storage Rooms are locked and can only be opened by members from the
[Residents' Council](/page/board/council/).

It will be possible to have the Storage Rooms opened every Tuesday at 19:00, but
only with agreement from the [Residents' Council](/page/board/council/).

At the agreed time you must have brought your stuff by
[the basement](/content/page/facilities/basement/) under block 5. All things
must be marked with full name, apartment number and a phone number on which you
can be contacted.

The [Residents' Council](/page/board/council/) can be contacted by clicking the
button below:

{{< messengerbutton abkresidentscouncil >}}
