---
title: Sauna
bigimg: [{src: "/page/page-images/abk-photos/sauna.jpg", desc: ""}]
---

You can find the sauna in [the basement](/page/facilities/basement/) under
[the common house](/page/facilities/common/)/block 5.

Residents may use the sauna at any point. Children under 16 years have to be
accompanied by an adult over 18 years old. People that are not residents have to
be accompanied by a resident. 

Price: For _10,- kr_, the oven is turned on for 30 min, which is enough heat for
one hour.

## Instructions

It takes 20-30 min. for the sauna to get hot (~60C).

1. The oven is turned on when the chip is used on the machine which is placed in
   the stairway by the common house. The oven automatically turns off after 30
   min.
2. The watch on the left is set to the red "1".
3. The watch on the right the temperature. `VII` is the highest.

![Sauna instructions](/page/page-images/sauna_instructions.png)

## After Use

The benches and the floor have to be cleaned after use. You can use the squeegee
found in shower for cleaning.
The water bucket has to be turned over in the shower.
