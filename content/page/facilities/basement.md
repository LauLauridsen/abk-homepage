---
title: Basement
---

The area below [the common house](/page/facilities/common), where you can find
[the Fitness room](/page/facilities/fitness/),
[the Solarium](/page/facilities/solarium/), and
[the Sauna](/page/facilities/sauna/).

There is a table tennis table, which is _free_ for all residents to use! Pick
you favorite neighbor, and beat them in a round of ping pong. Or even better,
invite the whole corridor down for a round the table. There are bats and balls
free to use, but it is recommended you bring your own.

![Table Tennis](/page/page-images/abk-photos/bordtennis.jpg)
