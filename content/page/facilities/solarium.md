---
title: Solarium
bigimg: [{src: "/page/page-images/solarium.jpg", desc: ""}]
---

You can find the solarium in [the basement](/page/facilities/basement/) under
[the common house](/page/facilities/common/)/block 5.

For _10,- kr._ you will get _20_ minutes of sun. You pay with your chip. 

Remember to **clean up** after your self!
