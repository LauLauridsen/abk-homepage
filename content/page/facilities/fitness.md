---
title: Fitness Room
bigimg: [{src: "/images/abk-photos/fitness/fitness.jpg", desc: ""}]
---

In [the basement](/page/facilities/basement/) under
[the common house](/page/facilities/common/)/block 5 you will find the Fitness
room, which is _free_ to use for residents.
In order to access the Fitness Room, you have to use your chip. 

## Weight and Cardio Rooms

There are two rooms in the Fitness Area: The Weight Room, and the Cardio Room. 

### The Weight Room:

- Free-weights
- Squat racks
- A bench
- Other related items

### The Cardio Room:

- Rowing machine
- Treadmill
- Elliptical trainer/cross-trainer
- Exercise bike 
- Therapy balls 
- Body bars
- Jump ropes
- Rubber bands
- Back trainer
- Other related items. 

{{< gallery >}}
  {{< figure src="/images/abk-photos/fitness/fitness.jpg" >}}
  {{< figure src="/images/abk-photos/fitness/fitness2.jpg" >}}
  {{< figure src="/images/abk-photos/fitness/fitness3.jpg" >}}
  {{< figure src="/images/abk-photos/fitness/fitness4.jpg" >}}
{{< /gallery >}}

## New Equipment

New equipment is sometimes added, and you are welcome to contact the
[Residents' Council](/page/board/council/) if something is missing, defective,
or you have ideas or wishes for improvement. The more detailed you are, the
better.

## Security

Note that it is _logged_ who uses the exercise room to secure theft and
vandalism.

## Rules of Conduct

We kindly ask you to follow our rules, as it ensures the optimal training
experience for everyone.

## Responsibility

- Use of ABK-fitness is at you rown risk.
- The fitness is open for all residents. However, residents under the age of 16
  have to be supervised by an adult (over age of 18).

## Behaviour

We want everyone to have a good experience when they train in ABK-fitness.
Therefore, we expect a proper tone and mutual respect.

## Treatment of Equipment

- Our equipment must be treated with respect, and used only for the exercises it
  is intended for. Any free weight equipment must have contact with the floor
  before release.
- Be sure to put the equipment at its place after use.
- Equipment may not be moved from the room for which they are intended; weight-
  or cardio room.
- Share the machines during the break between sets - then it will be easier for
  everyone to use the machines.
- Equipment must be cleaned after use. Cleaning supplies can be found next to
  the entrance to ABK-fitness.

## Clothing

You have to wear workout clothes and clean shoes. Outdoor shoes are not allowed!
