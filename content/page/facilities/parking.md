---
title: Parking
bigimg: [{src: "/page/page-images/bike3.png", desc: ""}]
---

## Car parking

ABK offers ~70 parking spots only for residents. There is always spots to park
your car, at any time during the day.

## Bicycle parking

There are several options for bicycle parking at the college.

The _outdoor_ bicycle sheds are located outside blocks 7 and 3, as well as a
bicycle rack at the common house. If you want to park your bike _indoors_, there
are locked bicycle rooms in the basement under each of the three blocks. It is
also possible to park in the _workshop_ house next to block 7.

There is a _compressor_ in the basement under block 5 that you can use to pump
your bike tires. Another one can be found
[in the workshop](/page/facilities/work/).
The workshop has good conditions for repairing bicycles.
