---
title: Urban Gardens
bigimg: [{src: "/page/page-images/abk-photos/haver.jpg", desc: ""}]
---

The raised beds (højbeder) can be rented out to residents who have time and
desire to grow their own vegetables or herbs over the summer.

Each raised bed is divided into two "gardens" and a total of _16_ gardens will
be available. Every apartment at ABK can apply to rent _one_ garden.

The gardens are *free* to rent, but you must sign a contract with
[the Residents' Council](/page/board/council/).

The rental will be from 5. may to 1. december in the same year, and is free to
rent. When you rent a garden, you must abide by our rules (which are handed to
you), that will, among other things, say you commit yourself to cleaning and
cultivating the garden in the given time period. All the rules that apply is
described in the lease document provided by
[the Residents' Council](/page/board/council/).

The rental takes place according to the principle first-come, first served.
