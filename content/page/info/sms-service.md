---
title: SMS-service
bigimg:
  - desc: SMS
    src: /media/mobil-m-sms.png
---

A smart way to stay informed

Notification/ info is sent via SMS, e.g. before planned water outage or in case
of acute damage.
Sign up for SMS service can be done at Hasseris Boligselskabs website.

[hasseris-boligselskab.dk/beboer/til-og-frameld-sms-service/](https://hasseris-boligselskab.dk/beboer/til-og-frameld-sms-service/)
