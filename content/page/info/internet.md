---
title: Internet
bigimg:
  - src: /page/page-images/internetphoto.jpg
    desc: ""
---

The apartments have _100 Mb/s_ connection.
It costs _25kr/month_ and is paid through the rent. You can connect to the
internet via an Ethernet cable. If you want Wi-Fi, you will have to use your own
Wi-Fi router. The internet is always available.

## Router Guide

To set up your own router, you need a router and an Ethernet cable. We recommend
using a router that supports Wi-Fi 5 (802.11ac) or higher, and is capable of
1000 Mbit (1 Gbit). One such router is the **TP-Link Archer C6**, which this
guide will focus on.

### Setup

Begin with connecting the router to the outlet. You connect the router to the
internet using the Ethernet cable between the blue port labeled `internet`, and
the green port in the wall.

- If you want to set up your internet by cable, connect your device with another
  Ethernet cable in one of the ports on the router labeled `ethernet`.
- If instead the setup is done wireless, connect to the Wi-Fi manually using the
  `SSID` and `Password` printed on the router, usually on the bottom.

At this point you may be able to use your internet connection, since some
routers are preconfigured to have both Ethernet and Wi-Fi working when plugged
in. The **Archer C6** is not, therefore it needs to be configured by accessing
the router through a web browser.

### Configuring

To start configuring the router, type the routers address in a web browser (such
as Chrome, Firefox, Edge, etc.). For our router, the TP-Link, it is
[tplinkwifi.net](https://tplinkwifi.net). Some addresses will be numbers with
dots in between, which should be entered without anything else, for example
`249.152.93.197`. From here you will find a log-in page to your routers'
configuration page, where this router requires the same credentials as you might
previously have connected with wirelessly and proceed from there. If you
encounter any problems, either with this router or any other, be sure to find a
guide that suits your situation.

After you have set your routers settings to your liking, there should now be a
Wi-Fi Access Point that your devices can use. Additional settings such as giving
the Wi-Fi connection a unique name and password, can be configured using the
router's configuration page.

## Support

If your internet stops working, here are some steps, you can follow.

* See if your router is plugged in correctly.
* Restart your router
* Make sure your Ethernet cable is plugged in correctly and that the Ethernet
  cable is working properly
* Make sure there is not any dirt or the like in the wall port
* If your router is connected to the green outlet try switching to the red and
  vice versa

If your internet still does not work, or if you have any questions, you can
write to the [ABK-Net association](/page/board/abk-net/):
[net@abk-aalborg.dk](mailto:net@abk-aalborg.dk)

Or contact us on Facebook, by clicking the messenger button:

{{< messengerbutton abknetforening >}}
