---
title : Apartments
bigimg: [{src: "../../page-images/tv.jpg", desc: ""}]

---

The following site are made available by residents thereby the Kollegium can
not be made responsible for mistakes. Apartments rented though
studieboligaalborg.dk are non-furnished while apartments rented though the AAU
international office are sparsely furnished. For more information, such as rent
and size, see studieboligaalborg.dk.