---
title : Administration / Caretaker
---

Our current caretaker is:

- Hans Henrik-Skall
- Email: [1201vm@hasseris-boligselskab.dk](mailto:1201vm@hasseris-boligselskab.dk)
- Number: [98 14 48 09](tel:98144809)

## Caretaker roles

The day-to-day operation of the college is handled by the caretaker, a caretaker
assistant and a cleaning assistant. The daily operation includes: 

- Supervision and maintenance of the college's common areas and fixed
  installations
- Administrative tasks
- Defective toilets
- Leaking taps etc. 

## Office Hours

The caretaker's office hours and contact possibilities can be found outside
their office, and are listed below.

Monday to Friday: 

- `07:30` - `08:00` 
- `12:30` - `13:00`

The first Tuesday of the month:

- `14:30` to `15:30`.
