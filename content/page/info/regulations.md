---
title : Regulations / House Rules
---

The regulation rules are currently only in Danish.

## § 1. Husorden

Husorden for Arbejderbevægelsens Kollegium udformes af beboerrådet og vedtages
på generalforsamlingen samt godkendes af kollegiets bestyrelse.

## § 2. Naboforhold

Vis hensyn til medbeboerne på kollegiet, der bor mange, og vi skal alle kunne
være her.

## § 3. Støj

Der skal være ro efter kl. 24. Musik bør på alle tidspunkter spilles i et
moderat lydniveau. Der må kun bores i væggen i tidsrummet kl. 09-19.

## § 4 Anvendelse af fællesarealer og faciliteter.

Beboerrådet udformer og vedtager vedtægter for brug af fællesarealer og
faciliteter. Se opslagstavler og kollegiets hjemmeside. Der henvises bl.a. til
regelsæt for:
  - Fælleshus
  - Stillerum
  - Sauna
  - Motionsrum
  - Depotrum
  - Kollegiehaver
  - Affaldshåndtering
  - Gæsteværelser

#### Stk. 1. Gangarealer

Af hensyn til brandfare må der ikke henstilles ting på gangarealer (ej heller
vasketøj).
Der må ikke afholdes fest på selve gangene.

#### Stk. 2. Fællesstøvsugere

Støvsugere skal straks efter brug afleveres tilbage i skabet på gangen.
Fyldte støvsugerposer skal smides i container i affaldsrum. Hvis beboeren tager
sidste støvsugerpose, er denne ansvarlig for at hente nye poser hos
varmemesteren.

#### Stk. 3. Vaskeri

Må kun benyttes af voksne beboere (over 16 år)
Vaskeriet er lukket mandag kl. 07-10, pga. rengøring, og vasketøj skal derfor
fjernes inden mandag morgen.
Anvisninger for brug af vaskemaskiner, tørretumblere mv. skal følges.

#### Stk. 4. Cykler

Cykler skal placeres i cykelkældre, garage eller i de udendørs cykelstativer,
cykler placeret på andre arealer fjernes.
Hver beboer må maximalt opbevare _én_ cykel i cykelkælderen under blokkene.
Øvrige cykler henvises til garagen eller de udendørs cykelstativer. I cykelrum
hvor der er ophængningskroge, skal disse benyttes (øvrige henvises til garagen).
Arealer til cykelparkering må ikke benyttes til andre formål.

#### Stk. 5. Depotrum

Depot må benyttes af alle beboere i begrænset omfang til opbevaring af alm.
indbo.
Alle genstande skal være markeret med dato, navn og lejlighedsnummer.

#### Stk. 6. Kollegiets fælles udstyr

Udstyr der kan lånes på kollegiet, er kollegiets ejendom.
Alle beboere kan låne udstyret hos ansvarshavende eller varmemesteren. Se liste
over udstyr, der kan lånes på kollegiets hjemmeside.
Udlånt udstyr skal afleveres tilbage i samme stand som ved modtagelsen.

#### Stk. 7. Parkering af motorkøretøjer

Biler og motorkøretøjer skal parkeres på parkeringspladser
Vejene øst og nord for kollegiet er brandveje og må ikke benyttes til parkering.

#### Stk. 8. Garagen

Værkstedet må kun benyttes af voksne beboere over 16 år.
Værkstedet og garagen efterlades som modtaget, efterladte dele/genstande vil
blive fjernet uden ansvar. Cykler må kun parkeres i området med cykelstativer. 
Motorcykler og knallerter må kun parkeres i området uden cykelstativer.

## § 5. Husdyr

Der må ikke holdes husdyr på kollegiet. Stuekultur, dvs. fugle, fisk og lign. er
ikke husdyr.

#### Stk. 1. Besøgsdyr/Besøg af husdyr

Besøgsdyr er tilladt i perioder af maksimalt 24 timers varighed og besøg af
husdyr må ske maksimalt én gang per måned.
Besøgsdyr må ikke være til gene, eller på nogen måde være truende for kollegiets
øvrige beboere, i disse tilfælde påkalder beboerrådet sig ret til øjeblikkelig
fjernelse.

## § 6. Indgang og terrasse ved parterrelejligheder

Foran parterrelejlighederne skal arealerne holdes rene og fri for skrald og
affald.
De pågældende beboere har ansvar for snerydning foran deres lejligheder.

## § 7. Rygning

Rygning er ikke tilladt på kollegiets indendørs fællesarealer, herunder gange,
kældre, gæsteværelser og fælleshus. Det er ej heller tilladt at aske og smide
cigaretskodder ud af vinduer.

## § 8. Klager over overtrædelser af husordenen

Stridigheder skal forsøges løst parterne imellem uden tredjepersons medvirken.

 - Beboerrådet behandler kun skriftlige klager, som ikke kan løses parterne
   imellem.
 - Beboerrådet tager herefter stilling til det videre forløb af sagen ved først
   kommende beboerrådsmøde.

## § 9. Sanktioner

Grove og/eller gentagne overtrædelser af husorden kan medføre opsigelse af
lejemålet.
