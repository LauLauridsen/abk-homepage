---
title : Borrowing Equipment
bigimg: [{src: "/page/page-images/vacuum.jpg", desc: ""}]
---

ABK offers a variety of equipment you can borrow. They are all free to borrow,
but some require a deposit, which you will be given back once the equipment is
returned.

## Tools

Toolbox, drill, jigsaw, and eccentric grinder
_(Værktøjskasse, boremaskine, stiksav og excentersliber)_ is lent from
[the caretaker](/page/info/contact/).
Do not try to use your own impact drill _(slagboremaskine)_, the walls are made
of reinforced concrete - many drills can not handle this. You have to use a
rotary hammer (_borehammer_)!

You are only allowed to drill between `9:00` and `19:00`. There is a _100,- kr_
deposit when you borrow the drill.

## Jack and Car Ramp (Donkraft og bilramper)

These items are lent from the Resident's Council. If you want to repair you car,
it is recommended to do it by the washing space next to the garage. There is a
_100,- kr_ deposit when you borrow one of the items.

## Board Games

In the [common house](/page/facilities/common/) you can find a closet with many
board games. They are **free** to borrow. Remember to return the game when you
are finished. You gain access to the common house with your chip.

## Sewing Machine

The sewing machine is lent from the [Resident's Council](/page/board/council/).
There is a _100,- kr_ deposit. You will need image documentation, your
signature, and documentation of your apartment number and name. The loan period
is _48 hours_.

## Vacuum Cleaner

Vacuums are available for the residents. They are found in the closets in the
hallways, which are on the right side of the hallway.
[Contact the caretaker](/page/info/contact/) if you have any questions.
