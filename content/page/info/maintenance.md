---
title : Maintenance Rules
---

The Maintenance Regulations describe how you should maintain and keep your
apartment clean, what and how often you should do maintenance, how the apartment
is when you move in, as well as the rules for departure.

## Danish

{{< pdf "/pdf/Vedligeholdelsesreglement-2022.pdf" >}}

## English

{{< pdf "/pdf/Vedligeholdelsesreglement-engelsk-2022.pdf" >}}
