---
title: Trash Disposal
---

This page holds information on how to sort your trash, whether it can be
recycled, burned, or collected for special recycling. If there is still
uncertainty about certain waste,
[consider reading the pamphlet](#trash-disposal-pamphlet) which has extensive
information on the sorting.

## Garbage Sorting

You can find the waste disposal facilities right next to
[the common house](/content/page/facilities/common). Disposal of regular waste,
paper, cardboard, metal, glass, and hard plastic in the _Moloks_ (waste
containers) next to the [common-house](/page/facilities/common). Other waste is
sorted in the container yard, an enclosed area in the corner of the parking lot.

{{< figure src="/media/trash_map.png" >}}

### Regular waste

{{< figure src="/images/affald/restaffald.jpg" width="64px" >}}

Regular waste (restaffald) is the burnable waste from the household, as well as
the non-recyclable domestic waste.

What is not regular waste is the following:

1. Bottles
2. Paper and cardboard
3. Plastic
4. Batteries
5. Garden waste
6. Metal
7. Electronics

Hazardous materials and chemicals are also not allowed as regular waste. All
these other types of waste is to be sorted in different ways detailed below.

There are three containers for regular waste.

### Paper and cardboard

{{< row >}}
{{< figure src="/images/affald/papir.jpg" width="64px" >}}
{{< figure src="/images/affald/pap.jpg" width="64px" >}}
{{< /row >}}

All paper and cardboard that is recyclable is allowed in these containers.

The ones that are not allowed are the following:

1. Food wrapping
2. Pizza boxes
3. Dirty or greasy paper
4. Paper or cardboard from food waste
5. Food and drink cartons
6. Diapers and paper used for hygiene

These are sorted as [non-recyclable domestic waste](#regular-waste).

There are two containers for paper and cardboard waste.

### Metal, plastic, and food- and drink cartons

{{< row >}}
{{< figure src="/images/affald/metal.jpg" width="64px" >}}
{{< figure src="/images/affald/plast.jpg" width="64px" >}}
{{< figure src="/images/affald/mad-og-drikkekartoner.jpg" width="64px" >}}
{{< /row >}}

The following is sorted as metal, plastic, and food- and drink cartons waste:

1. Plastic bottles and cans
2. Plastic from food items
3. Plastic toys
4. Plastic bags
5. Metal cans
6. Foil trays
7. Metal lids
8. Utensils, knifes and pots.
9. Milk cartons and similar

Plastics with food waste is not allowed and is
[non-recyclable domestic waste](#regular-waste).

Chemicals and other hazardous materials, such as turpentine and gasoline.

Electronics, spray cans and other hazardous materials.

There is one container for metal, plastic, and food- and drink cartons waste.

### Glass

{{< figure src="/images/affald/glas.jpg" width="64px" >}}

Bottles and clean glass are allowed as glass waste, while capsules as well as
metal and plastic lids are not. These are instead sorted as
[metal and plastic](#metal-and-plastic).

There is one container for glass waste.

### Batteries and electronics

{{< row >}}
{{< figure src="/images/affald/batterier.jpg" width="64px" >}}
{{< figure src="/images/affald/elektronik.jpg" width="64px" >}}
{{< /row >}}

Electronics and all forms of small batteries, both rechargeable and disposable,
can be delivered in the container yard, the prior in the "electronics cage", the
latter in the "battery box".

Note that there is several hidden batteries in different kinds of electronics,
f.ex. smartphones, toys, Christmas cards, tie with lights, blinking children
shoes.

Batteries should not be thrown with other waste, so be careful sorting these
separately.

Electronics can be f.ex. household appliances, mobile phones and other IT,
televisions and printers

### Disposal of large metal, wood or plastic objects

If the Moloks are too small, they can be disposed of at the container yard.

## Trash disposal pamphlet

Below is the pamphlet given to new resident, but only issued in Danish.

{{< pdf "/media/affaldsguide_abk_2018-2022.pdf" >}}
