---
title : Cable TV
bigimg: [{src: "/page/page-images/tv.jpg", desc: ""}]
---

When you move into the dormitory, you are free to sign up for any cable TV
package of your choice. Note that you will be ordering and managing this
yourself.

Remember to unsubscribe from the provider you chose when moving out of the
dormitory.
