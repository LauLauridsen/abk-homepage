---
title: Moving In
---

ABK-Aalborg welcomes you as a new resident at the college!

## When can I move in?

Please note that it may take _up to 14 days_ from the date of acquisition until
the apartment is ready and ready to move in. On the other hand, you have your
apartment until the last day of the month when moving away.

## Faults and defects

When moving in, it must be checked whether there are faults or defects as well
as whether cleaning has been done. Errors and defects may be: 

- cracks in windows
- holes or marks in the floor covering
- faults on refrigerators, stove, contacts, sockets, taps etc.

Notification of faults and defects must be made **no later than 14 days** after
moving in on a form that is handed out at the time of moving in. The occupier is
liable for refurbishment if no damage or error has been reported. Therefore, it
is advisable to note too much rather than too little on the form.
[The caretaker](/page/info/contact/) then assesses the notified.

It is generally true that faults must immediately be reported to
[the caretaker](/page/info/contact/), it can be problems with: 

- faucets
- toilet 
- drains
- refrigerator
- stove
- doors
- windows among other things

If faults or defects are discovered in one of the common rooms,
[contact the resident council](/page/board/council/).

{{< messengerbutton abkresidentscouncil >}}

## Drilling

Drilling in the wall is allowed from 9:00 to 19:00, and
[equipment can be borrowed](/page/info/equipment/).

## Post Danmark

Relocation must be reported to _Post Danmark_ and the _Folkeregisteret_.
Further information on maintenance can be found in the
[maintenance regulations](/page/info/maintenance/).
