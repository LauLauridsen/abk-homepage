---
title : Kro Aften
bigimg: [{src: "/page/page-images/balloons.jpg", desc: ""}]
---
The _'Kro Aften'_ is a theme-party that is held each semester. 
The three blocks: 3, 5, and 7 each plan and hosts _Kro Aften_ on rotation.
It is the responsibility of the block committee to plan the parties.

The parties are for all residents from the dormitory.

The profits from the party goes to each block's "gangkasse".
The Residents' Council is responsible for each of the three "gangkasser".
