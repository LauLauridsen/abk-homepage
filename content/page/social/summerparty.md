---
title : Summer Party
bigimg:
  - src: /media/summer-party.jpeg
---

Each year ABK hosts a summer party for the old and new residents.
It usually takes place in the beginning of September in order to get as many new
residents as possible to join us.

The primary goal of the Summer Party is to get to know each other which is why
this first part of this event is only for current ABK residents.

The party is split into three parts:

## 1. Summer activity

The party starts with an outdoors activity. 
In the past we have played beer-related games, and had a slip-and-slide in the
park.

## 2. Grill and Dinner

After the summer activity, we grill outside
[the common house](/page/facilities/common/) and eat together.

## 3. Party

The rest of the evening is for the party in
[the common house](/page/facilities/common), where the bar is open!
Non-ABK residents are welcome to join the rest of the evening.
