---
title : Friday Bar
bigimg: [{src: "/page/page-images/abkbar.jpg", desc: ""}]
---

Most Fridays, [ABK-Bar](/page/board/bar) hosts a Friday bar in
[the common house](/page/facilities/common/).
Residents and guests are welcome.
The Friday bar usually starts at _20:00_ and ends late in the evening (or in
some cases early in the morning).

## What can we buy?

Draft beer, bottled beer, drinks, shots, and soft-drinks can be bought at
student friendly prices!

## What can we do?

You can talk with your neighbors, play pool, foosball, board games, drinking
games, beer pong, and a lot of other activities.
