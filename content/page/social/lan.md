---
title : LAN-Party
bigimg: [{src: "/page/page-images/keyboard.jpeg", desc: ""}]
---

There are _plenty_ of gamers in ABK. [ABK-Net](/page/board/abk-net) sometimes
hosts a LAN-parties for the residents. This event takes place in the
[common-house](/page/facilities/common/), during which candy and soda is
consumed in great quantities, with a good deal of gaming. If you have an
interest in board games or computer games, and are looking for some gaming
buddies, [feel free to contact ABK-Net](/page/board/abk-net/#contact)!
