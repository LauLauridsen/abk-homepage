---
title : Christmas Party
bigimg: [{src: "/page/page-images/christmas.jpg", desc: ""}]
---

Each year, ABK hosts a Christmas party for the residents. The party starts with
a _Danish Christmas buffet_ were we all eat and drink together. Usually a lot of
_snaps_ is consumed throughout the party.
Some years, a group of residents has "brewed" their own snaps, which is usually
a huge hit!

Later in the evening we will play the "parcel gift game" where we can win small
funny gifts. The rest of the evening is party time.
