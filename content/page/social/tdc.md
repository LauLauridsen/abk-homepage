---
title : Tour de Chambres
---

## What is TDC?

Tour de Chambres (or abbreviated TDC) is a tradition for almost all dormitories.
Here at ABK it happens that residents visit each other's apartments and get a
'small one'. The purpose of a TDC is to _get to know your neighbors better_, get
around each other's rooms, consume a lot of alcohol and generally just party.
There are around 1-2 **TDCs** each year. 

## What happens in TDC?

A TDC usually starts with a _common_ dinner where we eat food together while the
first drinks are consumed. Subsequently, we split the participants into groups
and figure out which order we visit each other. The rest of the evening then
goes to visit the individual participants' rooms, where each participant serves
a drink equal to _approx. 1 item per person_ to the other participants.

Once all rooms have been reached, there is a tradition that those who have
stayed out all the way gather and parties on in the
[common house](/page/facilities/common/), where the bar will be open.

## Theme

Traditionally, TDC often has an overall theme such as different movies, music
genres, sports, countries or the like. Here, the individual participants can be
dressed appropriately to the theme, play appropriate music in the room and
possibly have the flavors and drinks to suit the theme.

An example can be a sports theme where a participant chooses to host the
football theme. Here, the costume can be a player jersey, "klaphat" etc. and in
the room you can hang the Denmark flag, play Re-Sepp-Ten, and possibly show
football on the TV.

However, it is up to you as a participant how much time you spend on your theme,
but the more people make of it, the more fun it is for everyone.
