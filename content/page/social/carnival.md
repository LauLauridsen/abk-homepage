---
title: Aalborg Carnival
bigimg: [{src: "/page/page-images/abkkarneval.png", desc: ""}]
---

Every year, [Aalborg Karneval](https://www.aalborgkarneval.dk/) holds a large
carnival which is the largest carnival in North Europe.

## Breakfast and Carnival

[ABK-Bar](/page/board/bar/) hosts a carnival event so that all residents of ABK
and their friends can walk _together_ in the Aalborg Carnival.
The residents meet in [the common house](/page/facilities/common/) where
breakfast is served, and the bar will be open.

When we all are stuffed (and perhaps drunk?) we will go together to the city
center and join the parade through lovely Aalborg!

## After the Carnival

Sometimes after the Carnival, the grill is fired up. Those who want, eats
together outside the common house. 
After dinner, we continue the party in the common house, although the ABK-Bar
may not be open - so bring your own drinks.
