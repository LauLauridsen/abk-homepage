---
title : Tour de Kollegie
bigimg: [{src: "/page/page-images/sohngaardholmsparken.png", desc: ""}]
---

## What is TDK?

Tour de Kollegie (or abbreviated TDK) is a tradition for ABK and the other three
nearby dormitories (Aalborghus, Blegkilde and Aalborgkollegiet). The purpose of
a TDK is to _get to know your dormitory "neighbors" better_, visit each other's
dormitories, consume a lot of alcohol and generally just party. There is about
**1 TDK** a year. 

## What happens in TDK?

All participants from ABK go as a group and walk to the first dormitories. Each
dormitory hosts a party in their common area with drinks and activities. We stay
in one dormitory _one to two hours_ and then walk to the next dormitory.
