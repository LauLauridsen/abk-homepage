Arbejderbevægelsens Kollegium is located on the top of 
_Sohngårdsholmsbakken_, which is a nice and quiet area and right next to the
beautiful _Sohngårdsholms Slotspark_. It is close to the city center and AAU
Campus, and well served by public transport, which makes the Kollegium
attractive for students.

The apartments at Arbejderbevægelses Kollegium are assigned through
[Studiebolig Aalborg](https://studieboligaalborg.dk)
and the International office at Aalborg University.

Address: Borgmester Jørgensensvej 3 -7, 9000 Aalborg
