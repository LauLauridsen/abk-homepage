---
title: Update in Fitness room
subtitle:
date: 2019-05-15
author: Residents' Council
tags:
  - Facilities
  - ResidentsCouncil
---

Today, some new equipment have been added to our collection, a new punching bag and new tricep barbell. In some time there have been problems with the different machines, but now every machine have been though a service check and thereby working a lot better. Specially the treadmill, cross trainer and the indoor bike. 

![Fitness - Triceps](/page/page-images/tricep.jpg)
![Fitness - Punching Bag](/page/page-images/punching.jpg)
![Fitness - Strips](/page/page-images/strips.jpg)
