---
title: "Oprydning af Depotrum | Emptying the Storage room "
date: 2022-11-21T09:31:46.958Z
author: Residents' Council
tags:
  - Service Announcement
---
I aften vil samtlige umærkede genstande i depotrummet blive dokumenteret med billeder, se link i bunden. 

Genstande som stadig er umærkede i februar vil blive kasseret. Hvis du som beboer har genstande stående i depotrummet som er umærkede, kontakt Beboerrådet på Facebook eller via mail: [info@abk-aalborg.dk](info@abk-aalborg.dk)

\---

Tomorrow, all unmarked objects in the storage room will be documented with pictures, see link at the bottom. Items still unmarked in February will be discarded.

If you as a resident have items left in the storage room that are unmarked, contact the Residents' Council on Facebook or via email: [info@abk-aalborg.dk](mailto:info@abk-aalborg.dk)

\---

<https://1drv.ms/u/s!AqJ0ov3FtfjKgtMuenann02TiZnh8Q?e=3QC2rw>