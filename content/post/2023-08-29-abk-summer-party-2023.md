---
title: ABK SUMMER PARTY 2023
date: 2023-08-29T21:37:20.287Z
author: ABK-Bar
tags:
  - Event
---
![ABK Summer Party-poster](/media/sommerfest-feed.jpg)

[TILMELDING VIA GOOGLE FORMS INDEN D. 6 SEPTEMBER:](https://forms.gle/LgZbPvW2j3K77qFr7?fbclid=IwAR1T6p2nAPR70FzS9g385MTkQpmWO506HGKPQ_IFh4ik_ZXfbcaOaqG9Vls)

Semesteret er begydt igen, sommeren er ved at være ovre og de mørke dage banker på døren. MEN inden vi siger helt farvel til sommeren, så skal vi selvfølgelig have en sommerfest!\
\
ABK inviterer derfor til sommerfest d. 9. september kl. 15:00 i fælleshuset. Find dit gode humør frem, tag dine naboer i hånden og stift bekendtskab med dine medkollegianere.\
\
Festen starter kl. 15:00, hvor der vil være nogle sjove aktiviteter til at sætte gang i festen. Det er selvfølgelig frivilligt, men vi opfordrer alle til at deltage i dem ![😃](https://static.xx.fbcdn.net/images/emoji.php/v9/t51/1/16/1f603.png) Når vi nærmere os aftenen, vil der blive tændt op i grillen og vi griller kl. 19! ABK-baren sørger for salater og brød, så det eneste du/I skal medbringe er kød (eller andet) til grillen. Det er ikke muligt at opbevare maden i fælleshuset inden aftensmaden, hvorfor der vil blive afsat tid til afhentning af maden i egen lejlighed efter aktiviteterne.\
\
Baren vil være åben hele aftenen, så der vil være mulighed for at købe øl, sodavand og drinks!\
Gæster udefra er velkomne fra kl. 20:30!\
\
Tilmeldingen sker via Google Forms inden d. 6. september\
\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\
[SIGN-UP THROUGH THE LINK BEFORE THE 6th OF SEPTEMBER:](https://l.facebook.com/l.php?u=https%3A%2F%2Fforms.gle%2FLgZbPvW2j3K77qFr7%3Ffbclid%3DIwAR06H3-7j7dEDXm6fZYr8D2ZQVlOaFx-h6zKEtnCBboZTD296I7NEpkeTLk&h=AT2b70cXA6TNDFv4h2NuiZH1HxjE4VbAOc8tAgEIKX67Cm16LFzl7mQFEdie4vuX4YgsMjicZO-kJNaUTbqtQFxdtgjkiVp17UJzASPtRVkxHVUsxPMUzrAIAaP6&__tn__=q&c[0]=AT05vI2_p5AwbgJZPl5DTMZGTa8J25yo3s1lWYGnM3ASpnoS0I3qxbDEyft1sMrevDDLJ4FhKvzZSSMeCr5ifxrKu8YQwEqsratWwjr0i-objPubI5heL0sxxLBi_85xglc)

On September the 9th, this year’s summer party takes place! So, gather your neighbors and come meet your fellow residents\
\
The party starts at 15:00. Here there will be some fun activities to get the party started. We encourage everyone to participate in the fun activities we have planned!\
\
In the evening, we eat together, and the grill will be warm. We provide salads and bread, so you only have to bring meat (or something else) for the grill. The grill will be ready at 19:00. It is not possible to store your food in the common house before dinner, which is why time will be set aside for getting your food in your own apartment after the activities\
\
The bar will be open the entire evening, so you can buy beer, drinks and soda!\
Guests not living in the dorm are welcome from 20:30!\
\
You sign-up through this link latest af the 6th of september