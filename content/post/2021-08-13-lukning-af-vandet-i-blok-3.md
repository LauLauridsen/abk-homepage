---
title: Lukning af vandet i blok 3
date: 2021-08-13T11:35:04.191Z
author: Administration / Caretaker
tags:
  - Service Announcement
  - Bathroom Renovation
---

### Danish
I forbindelse med opstart på renovering af badeværelserne vil der **mandag d. 16.08.2021 mellem kl. 08.00-12.00** være lukket for vandet i blok 3.

Vi beklager meget de gener dette måtte medføre.

### English
Due to the renovation of the bathrooms we have to close the water in block 3 on **Monday 16.08.2021 between 08.00-12.00 (24h format)**.

We are very sorry for any inconvenience this may cause.
