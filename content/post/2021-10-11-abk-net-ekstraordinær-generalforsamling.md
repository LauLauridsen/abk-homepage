---
title: ABK-NET ekstraordinær generalforsamling
subtitle: ABK-NET extraordinary general assembly
date: 2021-10-11T18:30:29.612Z
author: ABK-NET
tags:
  - ABK-NET
  - Event
attachment: /media/ekstraordinr_generalforsamling_-indkaldelse-_-_november.pdf
---

I den vedhæftede fil findes en indkaldelse til ekstraordinær generalforsamling i ABK-Net. Mødet finder sted **d. 2-11-2021.**

In the attached file an invitation to an extraordinary general assembly in ABK-NET can be found. The meeting takes place **2-11-2021.**
