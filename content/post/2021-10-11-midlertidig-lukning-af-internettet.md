---
title: Midlertidig lukning af internettet
date: 2021-10-11T17:20:53.151Z
author: ABK-NET
tags:
  - Service Announcement
---

Da ABK-Net skal opdatere internettet vil det være nede mandag d. 18 oktober og mandag d. 25 oktober fra kl. 16-00. Vi beklager de gener det måtte medføre. 

Hilsen ABK-Net



Due to ABK-Nets work upgrading the internet, there will be some downtime Monday the 18 of October and Monday the 25 of October. We are sorry for any inconveniences.

ABK-Net
