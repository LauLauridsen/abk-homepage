---
title: "Manual: Danfoss Icon rumtermostat"
subtitle: Danfoss Icon rumtermostat
date: 2022-03-04T10:27:09.115Z
author: Administration / Caretaker
tags:
  - Bathroom Renovation
attachment: /media/danfoss-icon-rumtermostat.pdf
---

Here you can read the manual for your room sensor in the bathroom.
