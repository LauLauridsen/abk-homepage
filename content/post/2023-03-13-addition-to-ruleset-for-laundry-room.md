---
title: Addition to ruleset for Laundry Room
date: 2023-03-13T11:00:35.113Z
author: Residents' Council
tags:
  - Facilities
---
**OBSERVER**

På grund af støjniveau, må tørretømbler 7 ikke benyttes mellem kl. 23 og kl. 7.

Overtrædelse af dette bryder [Husordningsreglerne](https://abk-aalborg.dk/page/info/regulations/#stk-3-vaskeri).

\---

**ATTENTION**

Due to excessive noise, dryer 7 is not allowed to operate between 11 PM and 7 AM.

Doing so violates the [Regulations](https://abk-aalborg.dk/page/info/regulations/#stk-3-vaskeri).