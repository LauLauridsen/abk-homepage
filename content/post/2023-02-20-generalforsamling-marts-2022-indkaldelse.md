---
title: Generalforsamling marts 2023 Indkaldelse
subtitle: ABK-NET Generalforsamling og Infoaften
date: 2023-02-20T16:40:03.438Z
author: ABK-NET
tags:
  - Event
attachment: /media/2023-3-07-indkaldelse-generalforsamling.pdf
---
ABK-NET Generalforsamling og Infoaften
---
Tirsdag den 7. marts kl. 18.30 i fælleshuset.
Læs mere i indkaldelsen i den vedhæftede fil.

ABK-NET General Assembly and Info Night
---
Tuesday the 7th of March at 18:30 in the common house.
Read more in the invitation in the attached pdf-file.