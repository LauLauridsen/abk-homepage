---
title: Generalforsamling marts 2024 Indkaldelse
subtitle: ABK-NET Generalforsamling og Infoaften
date: 2024-02-19T16:32:36.201Z
author: ABK-NET
tags:
  - Event
attachment: /media/2024-03-05-indkaldelse-generalforsamling.pdf
---
## ABK-NET Generalforsamling og Infoaften

Tirsdag den 5. marts kl. 18.30 i fælleshuset.
Læs mere i indkaldelsen i den vedhæftede fil.

---

## ABK-NET General Assembly and Info Night

Tuesday the 5th of March at 18:30 in the common house.
Read more in the invitation in the attached pdf-file.