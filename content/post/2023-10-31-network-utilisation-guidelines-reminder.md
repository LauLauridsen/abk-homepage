---
title: Network Utilisation Guidelines Reminder
subtitle: Official Communication Regarding the Unauthorized Distribution of
  Copyrighted Content
date: 2023-10-31T14:12:12.456Z
author: ABK-NET
tags:
  - Service Announcement
---
Dear residents,

We have recently been informed by DKCERT about the unauthorized sharing of copyrighted material. We would like to take this opportunity to remind all our valued residents that it is strictly prohibited to use our network for such activities. It is important to be aware that violating these rules can lead to severe consequences, including being suspended from our network and potential involvement of law enforcement.

Thank you for your attention and cooperation in maintaining a lawful residency.

Best regards,

ABK_NET