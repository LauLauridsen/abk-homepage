---
title: Fælleshuset åbner/common house Opens
date: 2021-08-18T17:48:39.199Z
author: Residents' Council
tags:
  - Facilities
---

Juhuu!! Vi har fået grønt lys til at åbne og bruge fælleshuset igen. Dette sker som efterfølge, at de sidste reskriktioner om areal- og afstandskrav udfaset fra den 14.8.



Hvilket betyder at fælleshuset og stillerummet (ved postkasserne i blok 5) bliver åben til normal brug og snart vil fredagsbar blive genindført når den er blivet klar. Regeringens nuværende retningslinjer gælder stadig, at vi passer på hinanden og spritter af. Derfor, vil der stå sprit rundt omkring til hænder og sprit til border.

* Fredagsbaren vil åbne så snart det praktiske er på plads.
* Motionsrummet er stadig lukket, men forventes at åbne i begyndelsen af september.
* Bordtennis er væk, sådan der er plads til flere borde til dem som bliver generet af byggestøj.

### English

WUHUU!! We have got green light to open the common house and the study room (at the mailboxes in block 5). This is possible as the last restriction about area/distance has phased out.



We can thereby use these areas, but still follow guidelines from the government. Respect each other and use hand-sanitizer. There will be hand-sanitizer at multiple places in the common house and possible to disinfect tables. Additional:



* The Friday-bar will open as soon as all the practical are in place.
* The fitness room is sadly still closed. Expected to open in the beginning of September.
* The ping-pong table is gone to make space for more tables, so people can find a quit place while the conduction of the bathrooms is happening.
