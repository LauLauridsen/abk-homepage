---
title: "Internet is down (Update: Connection is back)"
subtitle: Maintenance at AAU
date: 2021-04-22T08:21:23.556Z
author: ABK-NET
tags:
  - Service Announcement
---

The internet at ABK is currently down.

This is because AAU is doing maintenance, from where we get our internet connection.
We were not informed of this beforehand.

This means we cannot do anything before the maintenance is complete, and it is expected to take 1 to 1.5 hours.

Update (11:48): 

The internet is now back!
