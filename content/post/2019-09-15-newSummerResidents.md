---
title: Welcome to New Residents & Summer Party
subtitle: Drill only between 9:00 and 19:00
date: 2019-09-15
author: Residents' Council
tags:
  - ResidentsCouncil
---

Welcome to the new residents ABK typically gets at summertime, we can't wait to meet you :)

At this time, there is usually quite a few complaints about drilling, so please remember that **drilling is only allowed between 9:00 and 19:00**.

For new residents it is highly recommended reading the other pages on the website, especially the "practical info for residents" section containing info about [regulations](/page/info/regulations/), [moving in](/page/info/moving/), [maintenance rules](/page/info/maintenance/), and [trash disposal](/page/info/trash/) among other information.

If you can't read danish (understandable ;) ), use a translation service of your choice or ask the [Residents' Council on Facebook](https://m.me/abkresidentscouncil?ref=abk_website) if you are in doubt about what is- and is not allowed.

## Summer Party
This year's [summer party](/page/social/summerparty/) is coming up and is always great fun, read more about this on [ABK's Facebook group](https://www.facebook.com/groups/abkkollegium/).

In case you miss the summer party, there are other events on most Fridays, and the Facebook group is the primary way your fellow residents communicate, so it is beneficial to join.

That is it, have a nice day!
