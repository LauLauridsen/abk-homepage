---
title: Power outage in block 5
date: 2021-09-07T09:31:11.683Z
author: Administration / Caretaker
tags:
  - Service Announcement
---

Unfortunately we have to turn off the power for approx. 45 min from kl. 13.00 to Day in Block 5.

We apologize for the inconvenience and the short notice.
