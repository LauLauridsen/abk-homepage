---
title: "The internet connection is down. UPDATE: It's back up!"
date: 2021-09-07T11:44:10.661Z
author: ABK-NET
tags:
  - Service Announcement
---

Due to the power outage the internet is currently unavailable. 

We are working as hard as we can to fix this and apologize for any inconvenience this may cause.

We will update this post as soon as we fix it.

**UPDATE:** The connection *should* now be back up for all residents.

The internet may operate in a degraded state, but we will be fixing it soon :)
