# ABK Homepage

![Build Status](https://gitlab.com/abk-aalborg/abk-homepage/badges/master/pipeline.svg)

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

**Table of Contents** _generated with [DocToc](https://github.com/thlorenz/doctoc)_

- [ABK Homepage](#abk-homepage)
  - [Folder Structure](#folder-structure)
    - [Static content](#static-content)
  - [Galleries](#galleries)
  - [Icons](#icons)
  - [Facebook Messenger Button](#facebook-messenger-button)
- [Hugo](#hugo)
  - [GitLab CI](#gitlab-ci)
  - [Building locally](#building-locally)
    - [Preview your site](#preview-your-site)
  - [GitLab User or Group Pages](#gitlab-user-or-group-pages)
  - [Did you fork this project?](#did-you-fork-this-project)
  - [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## How to Write Markdown

[Markdown Cheat Sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

## Folder Structure

```
.
|-- content/            (Contains pages and posts content)
|-- layouts/            (Contains templates and shortcodes)
|-- static/             (Contains static content)
|-- |-- images/
|-- |-- pdf/
|-- themes/
|-- .gitlab-ci.yml      (Gitlab CI configuration)
|-- .gitmodules
|-- config.toml         (Hugo configuration)
```

More information in the [Hugo Documentation - Directory Structure](https://gohugo.io/getting-started/directory-structure/)

### Static content

Static content such as images and pdfs belong in the `static` folder.
To reference an image from the static folder: `/images/theimage.png`

Example:

```markdown
![Fitness](/images/abk-photos/fitness2.jpg)
```

This will look for the image at the following path: `./static/images/abk-photos/fitness2.jpg`

**REMEMBER TO MANUALLY COMPRESS LARGE IMAGES BEFORE PUTTING THEM ON THE WEBSITE!**

If you do not compress the large images, it will increase the load time of the webpage significantly.

## Galleries

We are using [hugo-easy-gallery](https://github.com/liwenyip/hugo-easy-gallery) to create galleries.
To create a simple gallery, see the following example:

```
{{< gallery >}}
  {{< figure src="/images/abk-photos/commonhouse/fælleshus.jpg" >}}
  {{< figure src="/images/abk-photos/commonhouse/fælleshus2.jpg" >}}
  {{< figure src="/images/abk-photos/commonhouse/fælleshus_boardgames.jpg" >}}
  {{< figure src="/images/abk-photos/commonhouse/fælleshus_køkken.jpg" >}}
{{< /gallery >}}
```

More information can be seen on hugo-easy-gallery's [Github Page](https://github.com/liwenyip/hugo-easy-gallery)

## Netlify CMS(Content Management System) for editing pages

If you want to edit the ABK-pages on the live website, it can be done so with the CMS. Go to the hidden [/admin](https://abk-aalborg.dk/admin/) page and login with GitLab.
 - Posts are the front pages posts, which is used to give updates on ABK.
 - Board pages, Facilities Pages, Info Pages and Social Pages corresponds to the topics in the navbar at the top of the page.
 - Everything in the CMS are using [Markdown](https://www.markdownguide.org/cheat-sheet/)
 - Use Media in the top left corner to add images and other static content. When adding images through the CMS, try to avoid uploading very huge image files, as the CMS doesn't have an image optimizer to keep the pipeline speed high. Instead use either an online tool or [image-compressor](https://gitlab.com/abk-aalborg/image-compressor)

## PDF

We are using [PDFObject](https://github.com/pipwerks/PDFObject) to view documents inside web pages.
To integrate a PDF document, see the following example:

```
{{< pdf "/pdf/abk-bar-statues.pdf" >}}
```

More information can be seen on PDFObject's [GitHub Page](https://github.com/pipwerks/PDFObject)

## Icons

A shortcode has been created that makes it easy to insert icons in the text. We are using [MDI-Icons](http://code.meta-platform.com/assets/mdi/preview.html). A list of the available icons can be seen in the link.

To insert an icon in the text:

```
{{< mdi the-icon >}}
```

Where `the-icon` is replaced by the name of the icon without `mdi`. So if the icon is called `mdi-folder` in the list, you write:

```
{{< mdi folder >}}
```

## Facebook Messenger Button

A shortcode has been created to make Facebook Messenger Buttons, which has the following syntax:

```
{{< messengerbutton mypageid >}}                            // default ref
{{< messengerbutton pageid="mypageid" ref="myref" >}}       // custom ref
```

The `ref` field is used to customize the ref parameter of the link, which can be used by the page administrators [Facebook - m.me Links](https://developers.facebook.com/docs/messenger-platform/discovery/m-me-links/#parameters). It defaults to `abk_website`.

---

# Hugo

![Build Status](https://gitlab.com/pages/hugo/badges/master/build.svg)

---

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
2. [Install][] Hugo
3. Run `git submodule update --init --recursive`
4. Preview your project: `hugo server`
5. Add content
6. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][].

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

The theme used is adapted from http://themes.gohugo.io/beautifulhugo/.

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

You'll need to configure your site too: change this line
in your `config.toml`, from `"https://pages.gitlab.io/hugo/"` to `baseurl = "https://namespace.gitlab.io"`.
Proceed equally if you are using a [custom domain][post]: `baseurl = "http(s)://example.com"`.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

   Either that you have wrongly set up the CSS URL in your templates, or
   your static generator has a configuration option that needs to be explicitly
   set in order to serve static assets under a relative URL.

2. `Error: Error building site: [...]`:

   Did you remember to run: `git submodule update --init --recursive` before running `hugo serve`?

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
